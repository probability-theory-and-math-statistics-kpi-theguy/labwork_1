package com.kpi;

import java.io.*;
import java.util.*;

import static java.lang.System.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner scan = new Scanner(in);
        out.println("Write the numbers of registers:");
        int n = scan.nextInt();
        out.println("Enter the number of ranks:");
        int r = scan.nextInt();
        out.println("Write the number of selection");
        int s = scan.nextInt();
        Table table = new Table(n, r);
        int[] numberArr = new int[s];
        for(int i = 0; i < s; i++){
            numberArr[i] = table.GenerateSequence();
        }
        out.println("Our array with size of "+s+" is saved in file: С://generator.txt");
        File fil = new File("A://generator.txt");
        try {
            if(!fil.exists()){
                fil.createNewFile();
            }
            FileWriter file = new FileWriter(fil);
            for(int i = 0; i < numberArr.length; i++){
                file.write(toString(numberArr[i]));
            }
        }catch(IOException e){
            err.println(e.getMessage());
        }
        out.println("Now, try the tests about this generator:");
        Tests test = new Tests(numberArr);
        out.println("Frequency test is: " + test.Test1());
        out.println("Differential test is: " + test.Test2());
        out.println("Enter the number for rangs:");
        int rang = scan.nextInt();
        out.println("Rang test is: ");
        HashMap<String, Integer> rangtest = new HashMap<>(test.Test3(rang));
        for(HashMap.Entry<String, Integer> entry: rangtest.entrySet()){
            System.out.println("Ranks: " + entry.getKey() + " Values: " + entry.getValue() + ". Test is: "+ ((double)entry.getValue() / numberArr.length));
        }
        out.println("Complexity test is:" + test.Test4());
    }

    private static String toString(int i) {
        if(i == 1)
        return "1";
        else if( i == 0)
        return "0";
        return null;
    }
}
