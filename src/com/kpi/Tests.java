package com.kpi;

import java.util.*;

/**
 * Created by Yuriy on 11.09.2016.
 */

/**
 * Tests for checking the correct working of the generator
 */
public class Tests {
    /**
     * @param testingArr
     * The sequence that was generated
     */
    private int[] testingArr;

    //Setter
    public void SetTestingArr(int[] testingArr) {
        this.testingArr = testingArr;
    }
    //Getter
    public int[] GetTestingArr() {
        return testingArr;
    }

    /**
     * Constructor of this class
     * @param test
     * Sets the value of this sequence
     */
    public Tests(int[] test){
        SetTestingArr(test);
    }

    /**
     *Frequency test
     * @return
     * Result of this test
     */
    public double Test1(){
        int counter = 0;
        for(int i = 0; i < testingArr.length; i++){
            if(testingArr[i] == 1)
                counter++;
        }
        return ((double)counter / testingArr.length);
    }

    /**
     * Differentiated test
     * @return
     * Result of this test
     */
    public double Test2(){
        int sum = 0;
        for(int i = 1; i < testingArr.length; i++){
            sum = sum + (testingArr[i] ^ testingArr[i - 1]);
        }
        return ((double)sum / (testingArr.length - 1));
    }

    /**
     * Ranks test
     * @param n
     * The number of ranks that will be checked
     * @return
     * Result of this test
     */
    public HashMap<String, Integer> Test3(int n){
        HashMap<String, Integer> rangs = new HashMap<String, Integer>();
        String[] Keys = new String[(int) Math.pow(2, n)];
        for(int i = 0; i < (int) Math.pow(2, n); i++) {
            Keys[i] = toBinary(i, n);
        }
        String buf = "";
        int[] Values = new int[(int) Math.pow(2, n)];
        for(int i = 0; i < testingArr.length - n; i++){
            for(int j = 0; j < n; j++) {
                buf = buf + testingArr[j + i];
            }
            for(int k = 0; k < (int) Math.pow(2, n); k++){
                if(buf.equals(Keys[k])){
                        Values[k] = Values[k] + 1;
                }
            }
            buf = "";
        }
        for(int i = 0; i < Values.length; i++){
            rangs.put(Keys[i], Values[i]);
        }
        return rangs;
    }

    /**
     * Complexity test
     * @return
     * Result of this test
     */
    public int Test4(){
        int length = 0;
        int[] Cpolynom = new int[testingArr.length];
        Cpolynom[0] = 1;
        int[] Bpolynom = new int[testingArr.length];
        Bpolynom[0] = 1;
        int[] Bufpolynom = new int[testingArr.length];
        Bufpolynom[0] = 1;
        int x = -1;
        int sum = 0;
        for(int n = 0; n < testingArr.length; n++){
            int d = 0;
            for (int j = 0; j < length; j++){
                sum ^= (Cpolynom[j] * testingArr[n - j]);
            }
            d = testingArr[n] ^ sum;
            if(d == 1) {
                System.arraycopy(Cpolynom, 0, Bufpolynom, 0, testingArr.length);
                int NiX = n - x;
                for(int i = 0; i < testingArr.length - NiX; i++){
                    Cpolynom[NiX + i] ^= Bpolynom[i];
                }
                if(length <= testingArr.length / 2){
                    length = n + 1 - length;
                    x = n;
                    System.arraycopy(Bufpolynom, 0, Bpolynom, 0, testingArr.length);
                }
            }
        }
        return length;
    }

    /**
     * Method of translation of decimal number to binary, with a specific rank
     * @param i
     * A decimal number that tranlate
     * @param n
     * Capacity of this number
     * @return
     * Binary number as a string
     */
    private String toBinary (int i, int n){
        String res = Integer.toBinaryString(i);
        while(res.length() < n)
            res = "0" + res;
        return res;
    }
}
