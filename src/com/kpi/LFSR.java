package com.kpi;

/**
 * Created by Yuriy on 09.2016.
 */

import java.util.Random;

/**
 * Register LFSR built with exploded scheme
 */
public class LFSR {
    /**
     * @param Size
     * Length(grade) of the current register
     * @param PolynomicsArr
     * Array that contains the number of the polynomials
     * @param NumberArr
     * Numbers that randomly generates on this register
     */
    private byte[] polynom;
    private int size;
    private int[] numbers;

    //Setter

    public void SetPolynom(byte[] polynoms){
        this.polynom = polynoms;
    }
    public void SetNumbers(int[] numbers){
        this.numbers = numbers;
    }
    public void SetSize(int size) {
        this.size = size;
    }

    //Getter
    public byte[] GetPolynomicsArr(){
        return polynom;
    }
    public int[] GetNumbersArr(){
        return numbers;
    }
    public int GetSize() {
        return size;
    }


    /**
     * Create the constructor of the register, where located the generator for polynomial and for array of number
     * @param size
     * parameter, for taking the length of register
     */
    public LFSR (int size){
        Random rand = new Random();
        byte[]polynoms = new byte[size];
        for(int i = 0; i < polynoms.length; i++){
            polynoms[i] = (byte) rand.nextInt(2);
        }
        int[] numbers = new int[polynoms.length];


        for(int i = 0; i < polynoms.length; i++){
            numbers[i] = rand.nextInt(2);
        }
        SetSize(size);
        SetNumbers(numbers);
        SetPolynom(polynoms);
    }

    /**
     * Method, that makes the LFSR to produce randomly numbers
     * @return buffer
     * This statement returns the nuumber, that is generated internally
     */
    public int GenerateNumber(){
        int length = size - 1;
        int buffer = numbers[length];
        for(int i = length; i > 0; i--) {
            if (polynom[i] == 1) {
                numbers[i] = numbers[i - 1] ^ buffer;
            } else if(polynom[i] == 0) {
                numbers[i] = numbers[i - 1];
            }
            numbers[0] = buffer;
        }
        return buffer;
    }


}