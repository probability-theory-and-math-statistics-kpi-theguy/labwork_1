package com.kpi;

/**
 * Created by Yuriy on 04.09.2016.
 */

import java.util.*;

/**
 * Table, that consist of cells where placed randomly numbers, which are required for main sequance
 */
public class Table {
    /**
     * @param lfsr
     * Arraylist of registers, which makes a numbers for cells
     * @param size
     * Field, that indicates the number of these registers
     * @param table
     * Array of numbers, that made up this table
     * @param quantity
     * The number of bits used in registers
     */
    private ArrayList<LFSR> lfsr;
    private int size;
    private int[] table;
    private int quantity;

    //Setter
    public void SetSize(int s){
        this.size = s;
    }
    public void SetLfsr(ArrayList<LFSR> lfsr) {
        this.lfsr = lfsr;
    }
    public void SetTable(int[] table) {
        this.table = table;
    }
    public void SetQuantity(int quantity) {
        this.quantity = quantity;
    }

    //Getter
    public int GetSize(){
        return size;
    }
    public ArrayList<LFSR> GetLfsr() {
        return lfsr;
    }
    public int[] getTable() {
        return table;
    }
    public int getQuantity() {
        return quantity;
    }

    /**
     * Constructor of table, which record the random numbers 1 and 0
     * @param s
     * Parameter of the field size, which is about the number of registers
     * @param quan
     * Parameter of the field quantity, which shows a capacity of registers
     */
    public Table(int s, int quan){
        Random rand = new Random();
        int[] tabl = new int[(int) Math.pow(2, s)];

        for (int i = 0; i < tabl.length; i++){
            tabl[i] = rand.nextInt(2);
        }

        int num0, num1, num;
        do{
            num0 = 0;
            num1 = 0;
            num = 0;
            for(int i = 0; i < tabl.length; i++) {
                if (tabl[i] == 1)
                    num1++;
                else if (tabl[i] == 0)
                    num0++;
            }
            num = num1 - num0;
            if(num != 0)
            for(int i = 0; i < tabl.length; i++) {
                if ((num > 0) && (tabl[i] == 1)){
                    tabl[i] = 0;
                    break;
                }
                if ((num < 0)  && (tabl[i] == 0)){
                    tabl[i] = 1;
                    break;
                }
            }
        }while(num != 0);

        ArrayList<LFSR> registers = new ArrayList<LFSR>();
        for( int i = 0; i < s; i++){
            registers.add(new LFSR(quan + i));
        }

        SetLfsr(registers);
        SetTable(tabl);
        SetSize(s);
        SetQuantity(quan);
    }

    /**
     * Method, that generates the final sequence from the table
     * @return
     * At the output we have the number, that came out from the generation
     */
    public int GenerateSequence (){
        int[] sequence = new int[size];
        for (int i = 0; i < lfsr.size(); i++){
            sequence[i] = lfsr.get(i).GenerateNumber();
        }
        int tableCell = fromBinaryToDecimal(sequence);
        int result = table[tableCell];
        return result;
    }

    /**
     * Method that convers an array of binary numbers into decimal
     * @param b
     * Input array of binary numbers
     * @return
     * Decimal number from this array
     */
    private static int fromBinaryToDecimal(int[] b) {
        int result = 0;
        for(int i = 0; i < b.length; i++)
        {
            if (b[i] == 1)
            result += (int) Math.pow(2, i);
        }
        return result;
    }
}
